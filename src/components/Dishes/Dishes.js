import React, {Component} from 'react';
import Spinner from "../UI/Spinner/Spinner";
import {connect} from "react-redux";
import {fetchGetDishes} from "../../store/actions/dishesBuilder";
import {addDishes} from "../../store/actions/cartBuilder";

import './Dishes.css';


class Dishes extends Component {

    componentDidMount() {
        this.props.loadDishes();
    }

    render() {
        let dish = Object.keys(this.props.dishes).map(id => {
            return (
                <div key={id} className="dishes">
                    <img className="dish-image" src={this.props.dishes[id].img} alt={this.props.dishes[id].title}/>
                    <p className="dish-title">{this.props.dishes[id].title}</p>
                    <p className="dish-price">{this.props.dishes[id].price} сом</p>
                    <button className="add" onClick={() => this.props.addDishes({name: id, price: this.props.dishes[id].price})}>Добавить в корзину</button>
                </div>
            )
        });
        return (
            <div className="menu-block">
                <h4>Меню:</h4>
                <div className="menu">
                    {this.props.loading ? <Spinner/> : dish}
                </div>
            </div>

        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes,
    loading: state.dishes.loading
});

const mapDispatchToProps = dispatch => ({
    loadDishes: () => dispatch(fetchGetDishes()),
    addDishes: (order) => dispatch(addDishes(order)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);
