import React, {Component, Fragment} from 'react';
import Backdrop from "../Backdrop/Backdrop";
import {fetchPostOrder, modalClose} from "../../../store/actions/cartBuilder";
import {connect} from "react-redux";

import './Modal.css';

class Modal extends Component {
    state = {
      userName: '',
      userEmail: '',
      userPhone: '',
    };

    changeInputValue = event => {
        this.setState({[event.target.name] : event.target.value});
    };

    sentOrderHandler = () => {
      const user = {
          name: this.state.userName,
          email: this.state.userEmail,
          phone: this.state.userPhone
      }
      this.props.fetchPostOrder(user)
    };

    render() {
        return (
            <Fragment>
                <Backdrop show={this.props.show}/>
                <div
                    className="Modal"
                    style={{
                        transform: this.props.show ? 'translateY(0)': 'translateY(-100vh)',
                        opacity: this.props.show ? '1' : '0'
                    }}
                >
                    <h4>Укажите ваши данные для оформления заказа!</h4>
                    <input type="text" name="userName" value={this.state.userName} onChange={this.changeInputValue} placeholder="Введите ваше имя"/>
                    <input type="text" name="userEmail" value={this.state.userEmail} onChange={this.changeInputValue} placeholder="Введите ваш адрес"/>
                    <input type="number" name="userPhone" value={this.state.userPhone} onChange={this.changeInputValue} placeholder="Введите ваш номер телефона"/>
                    <button className="create-delivery" type="submit" onClick={this.sentOrderHandler}>Создать заказ</button>
                    <button className="close" onClick={this.props.modalClose}>X</button>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cart: state.cart.dishes,
});

const mapDispatchToProps = dispatch => ({
    modalClose: () => dispatch(modalClose()),
    fetchPostOrder: (user) => dispatch(fetchPostOrder(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
