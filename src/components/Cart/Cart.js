import React, {Component, Fragment} from 'react';
import Spinner from "../Dishes/Dishes";

import {connect} from "react-redux";

import './Cart.css';
import {deliveryOrder, deleteDishes} from "../../store/actions/cartBuilder";
import Modal from "../UI/Modal/Modal";

class Cart extends Component {
    render() {
        let order = Object.keys(this.props.dishes).map(order => {
            if (this.props.cart[order] < 1) return null;
           return (
               <div key={order} className="order">
                   <p className="order-name">{this.props.dishes[order].title}</p>
                   <p className="order-amount">X {this.props.cart[order]}</p>
                   <p className="order-price">{this.props.dishes[order].price * this.props.cart[order]} KGS</p>
                   <button className="delete" onClick={() => this.props.deleteDishes(order, this.props.dishes[order].price)}>Удалить</button>
               </div>

           )
        });
        return (
            <Fragment>
                <Modal
                    show={this.props.purchasing}
                    close={this.props.purchasing}
                />
                <div className="menu-cart">
                    {this.props.loading ? <Spinner/> : order}
                    <div className="price">
                        <p className="delivery">Доставка: 150 KGS</p>
                        <p className="total-price">Total price: {this.props.totalPrice} KGS</p>
                        <button
                            className="order-delivery"
                            disabled={!this.props.purchasable}
                            onClick={this.props.deliveryOrder}
                        >
                            Оформить заказ
                        </button>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes,
    cart: state.cart.dishes,
    totalPrice: state.cart.totalPrice,
    purchasable: state.cart.purchasable,
    purchasing: state.cart.purchasing,
});

const mapDispatchToProps = dispatch => ({
    deleteDishes: (name, price) => dispatch(deleteDishes(name, price)),
    deliveryOrder: () => dispatch(deliveryOrder())
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
