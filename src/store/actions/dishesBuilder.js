import axios from '../../axiosMenu';
import {DISHES_FAILURE, DISHES_REQUEST, DISHES_REQUEST_SUCCESS} from "./actionTypes";



export const dishesRequest = () => {
    return {type: DISHES_REQUEST};
};

export const dishesSuccess = dishes => {
    return {type: DISHES_REQUEST_SUCCESS, dishes};
};

export const dishesFailure = error => {
    return {type: DISHES_FAILURE, error};
};

export const fetchGetDishes = () => {
    return (dispatch, getState) => {
        dispatch(dishesRequest());
        axios.get('/menu.json').then(response => {
            dispatch(dishesSuccess(response.data));
        }, error => {
            dispatch(dishesFailure(error));
        })
    }
};
