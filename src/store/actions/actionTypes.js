export const ADD_DISHES = 'ADD_DISHES';
export const DELETE_DISHES = 'DELETE_DISHES';
export const DELIVERY_ORDER = 'DELIVERY_ORDER';
export const MODAL_CLOSE = 'MODAL_CLOSE';
export const DISHES_REQUEST = 'DISHES_REQUEST';
export const  DISHES_REQUEST_SUCCESS = 'DISHES_REQUEST_SUCCESS';
export const DISHES_FAILURE = 'DISHES_FAILURE';
