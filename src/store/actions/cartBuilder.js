import {ADD_DISHES, DELETE_DISHES, DELIVERY_ORDER, MODAL_CLOSE} from "./actionTypes";
import axios from "../../axiosMenu";
import {dishesFailure, dishesRequest, fetchGetDishes} from "./dishesBuilder";

export const addDishes = order => {
    return {type: ADD_DISHES, order}
};

export const deleteDishes = (name, price) => {
    return {type: DELETE_DISHES, name, price}
};

export const deliveryOrder = () => {
    return {type: DELIVERY_ORDER}
};

export const modalClose = () => {
    return {type: MODAL_CLOSE}
};

export const fetchPostOrder = (user) => {
    return (dispatch, getState) => {
        const order = {
            user: user,
            order: getState().cart.dishes
        };
        dispatch(dishesRequest());
        axios.post('/order.json', order).then(() => {
            dispatch(modalClose());
            dispatch(fetchGetDishes())
        }, error => {
            dispatch(dishesFailure(error));
        })
    }
};
