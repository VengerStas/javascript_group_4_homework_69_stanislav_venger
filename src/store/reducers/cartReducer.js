import {ADD_DISHES, DELETE_DISHES, DELIVERY_ORDER, DISHES_REQUEST_SUCCESS, MODAL_CLOSE} from "../actions/actionTypes";

const initialState = {
    dishes: {},
    totalPrice: 150,
    purchasable: false,
    purchasing: false,
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case DISHES_REQUEST_SUCCESS:
            let newDishes = {};
            Object.keys(action.dishes).forEach(dish => {
                newDishes[dish] = 0;
            });
            return {...state, dishes: newDishes};
        case ADD_DISHES:
        return {
                ...state,
                dishes: {...state.dishes, [action.order.name] : state.dishes[action.order.name] + 1},
                totalPrice: state.totalPrice + action.order.price,
                purchasable: true,
            };
        case DELETE_DISHES:
            if (state.dishes[action.name] === 0) return state;
            return {
                ...state,
                dishes: {...state.dishes, [action.name] : state.dishes[action.name] - 1},
                totalPrice: state.totalPrice - action.price,
                // purchasable: false,
            };
        case DELIVERY_ORDER:
            return {
                ...state,
                purchasing: true,
            };
        case MODAL_CLOSE:
            return {
                ...state,
                purchasing: false,
            };
        default:
            return state;
    }
};


export default cartReducer;
