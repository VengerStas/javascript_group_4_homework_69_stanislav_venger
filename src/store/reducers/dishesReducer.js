import {DISHES_FAILURE, DISHES_REQUEST, DISHES_REQUEST_SUCCESS} from "../actions/actionTypes";

const initialState = {
    dishes: {},
    loading: false,
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case DISHES_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DISHES_REQUEST_SUCCESS:
            return {
                ...state,
                dishes: action.dishes,
                loading: false
            };
        case DISHES_FAILURE:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};

export default dishesReducer;
