import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://menu-e8ed1.firebaseio.com/'
});

export default instance;
