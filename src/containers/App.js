import React, { Component } from 'react';
import './App.css';
import Dishes from "../components/Dishes/Dishes";
import Cart from "../components/Cart/Cart";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Dishes/>
        <Cart/>
      </div>
    );
  }
}

export default App;
